import request from '@/utils/request'

export default {
  getTodayInfo() {
    return request({
      url: '/realTime/get',
      method: 'get'
    })
  },
  getLedSwitchStatus() {
    return request({
      url: '/deviceGet/ledSwitch',
      method: 'get'
    })
  },
  getLast24Hours() {
    return request({
      url: '/history/lastDay',
      method: 'get'
    })
  },
  setLedSwitch(statues) {
    return request({
      url: '/deviceSet/switch',
      method: 'post',
      data: {
        deviceName: 'test',
        ledSwitch: statues
      }
    })
  },
  getLast24HoursTem() {
    return request({
      url: '/history/lastDayEchartsTemp',
      method: 'get'
    })
  },
  getLast24HoursHum() {
    return request({
      url: '/history/lastDayEchartsHum',
      method: 'get'
    })
  }
  ,
  getLast24HoursIllum() {
    return request({
      url: '/history/lastDayEchartsIllum',
      method: 'get'
    })
  }

}
